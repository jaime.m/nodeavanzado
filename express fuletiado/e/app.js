const express = require('express')
const server = express()
const port =2000

const {createReadStream} = require('fs')

server.get('/',(req,res)=>{
    res.send('hola mundo')
})



server.listen(port,()=>{
    console.log(`server running on port ${port}`);
  
})

//enviar video por stream! que brutalidad
server.use('video-stream',(req,res,next)=>{
    const filename='./public/video.mp4'
    res.writeHead(200,{
        "Content-Type":"video/mp4"
    })

    createReadStream(filename).pipe(res)
})